%% Compiler Analyzer tag2tig: DyALog 1.11.2
%% Date    : 12 21 2006 at 11:17:47
%% File "/home/bearn/clerger/Grammars/french-toy-tag-1.0.0/xtag_trees.tag"

:-tig(ads,left).
:-tig(sad,right).
:-tig(nppn,right).
:-tig(vppn,right).
:-tig(vpad,right).
:-tig(advp,left).
:-tig(an,left).
:-tig(na,right).
:-tig(vad,right).
:-tig(vvp,left).
:-tig(r0t,right).
:-tig(r0ta,right).
:-tig(r0tdn1,right).
:-tig(r0tn1,right).
:-tig(r1tn1,right).
:-tig(r1tn1pn2,right).
:-tig(r2tn1pn2,right).
:-tig(r0tn1pn2,right).
:-tig(r0tpdn1,right).
:-tig(r0tpn1,right).
:-tig(r1tpn1,right).
:-tig(r0ts1,right).
%%:-tig(ts1,left).
%%:-tig(w0ts1,left).
:-tig(npcoord,right).
:-tig(scoord,right).
:-tig(acoord,right).
:-tig(ncoord,right).

:-adjkind(a{},right).
:-adjkind(v{},right).
:-adjkind(n{},left).
:-adjkind(n{},right).
:-adjkind(vp{},left).
:-adjkind(vp{},right).
:-adjkind(np{},right).
:-adjkind(s{},left).
:-adjkind(s{},right).
:-adjkind(s{},wrap).
:-adjkind(_,no).

