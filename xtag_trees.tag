/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C)2000, 2003, 2004 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  xtag_trees.tag -- Trees for French Toy XTAG (Non Lexicalized)
 *
 * ----------------------------------------------------------------
 * Description
 *   First formatted by Francois Barthelemy
 * ----------------------------------------------------------------
 */

%% Statistics:
%%    24 families
%%    50 trees
%%    nb trees per family : distribution
%% nb trees:  1  2  3  4  5  6  7 
%% nb fam  : 16  0  3  2  2  0  1 

%% desactivated trees:
%%     svcl Fam svcl

%% look for WARNINGS

:-include 'xtag_header.tag'.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Family npdn {npdn}

tag_tree{ name => npdn,
	  family => npdn,
          tree=> tree id=np_
	and bot=np{num => X0, gen => X1, wh => X2}
	at np(
	      top=d{num => X0, gen => X1, wh => X2}
	     at d,
	      id=n_
	     and top=n{num => X0, gen => X1}
	     at <> n
	     )
	}.

%% Family np {np}

tag_tree{ name => np,
	  family => np,
	  tree=> tree id=np_ at <> np
	}.

%% Family a {a}

tag_tree{ name => a,
	  family => a,
	  tree=> tree
	id=ap_
	and bot=ap{num => X0, gen => X1, restr => X2}
	at ap( top=a{restr => X2}
	     and bot=a{num => X0, gen => X1}
	     at <> a
	     )
	}.


%% Family d {d}

tag_tree{ name => d,
	  family => d,
          tree=> tree
	id=d_
	at <> d
	}.

%% Family n {n}

tag_tree{ name => n,
	  family => n,
          tree=> tree <> n
	}.

%% Family p {p}

tag_tree{ name => p,
	  family => p,
          tree=> tree <> p
         }.

%% Family ads {ads}

tag_tree{ name => ads,
	  family => ads,
          tree=> auxtree s( <> ad,
			    bot=s{quest => (-)}
			  at *s
			  )
	}.

%% Family sad {sad}

tag_tree{ name => sad,
	  family => sad,
	  tree=> auxtree s( *s, <> ad)
	}.

%% Family nppn {nppn}

tag_tree{ name => nppn,
	  family => nppn,
	  tree=> auxtree np( *np,
			     pp( <> p, np )
			   )
	}.

%% Family vppn {vppn}

tag_tree{ name => vppn,
	  family => vppn,
	  tree=> auxtree
	bot=VP::vp{mode => X0, num => X1, gen => X2, pers => X3}
	at vp(
	      bot=VP
	     at *vp,
	      pp( <> p,
		  id=np_
		at np
		)
	     )
	}.

%% Family vpad {vpad}

tag_tree{ name => vpad,
	  family => vpad,
	  tree=> auxtree
	bot=VP::vp{mode => X0, num => X1, gen => X2, pers => X3}
	at vp(
	      bot=VP
     	 at *vp,
	      <> ad
	     )
	}.

%% Family advp {advp}

tag_tree{ name => advp,
	  family => advp,
	  tree=> auxtree
	bot=VP::vp{mode => X0, num => X1, gen => X2, pers => X3}
	at vp( <> ad,
	       bot=VP
	     at *vp
	     )
	}.

%% Family an {an}

tag_tree{ name => an,
	  family => an,
           tree=> auxtree
	bot=n{gen => X1, num => X0}
         at n(
	      bot=a{num => X0, gen => X1 }
	     at <> a,
	      bot=n{num => X0, gen => X1}
	     at *n
	     )
	}.

%% Family na {na}

tag_tree{ name => na,
	  family => na,
	  tree=> auxtree
	bot=n{gen => X1, num => X0}
	at n(
	     id=n_
	    and bot=n{num => X0, gen => X1}
	    at *n,
	     bot=a{num => X0, gen => X1}
	    at <> a
	    )
	}.

%% Family vad {vad}

tag_tree{ name => vad,
	  family => vad,
	  tree=> auxtree
	bot=v{mode => X0, num => X1, gen => X2, pers => X3}
	at v(
	     bot=v{mode => X0, num => X1, gen => X2, pers => X3}
     	at *v,
	     <> ad
	    )
	}.

%% Family svcl {svcl}
%% Warning: double anchors and no related lemma

/*
tag_tree{ name => svcl,
	  family => svcl,
	  tree=> auxtree -s( <> v,
			     <> cl,
			     bot=s{inv => (+}}
			   at s( comp( [que] ),
				 bot=s{inv => (-), quest => (-)}
			       at *s
			       )
			   )
	}.
*/

%% Family vvp {vvp}

tag_tree{ name => vvp,
	  family => vvp,
	  tree=> auxtree
	bot=vp{mode => X0, num => X1, pers => X2}
	at vp(
	      top=v{mode=>X0,num=>X1,pers=>X2} at
	     <> v,
	       id=vp_
	     %% and bot=vp{mode => X0, num => X1, pers => X2}
	     at *vp
	     )
	}.

%% Family t {t,w0t,r0t}

tag_tree{ name => t,
	  family => t,
           tree=> tree
	bot=s{mode => X2, inv => (-)}
	at s(
	     id=np_0
	    and top=np{num => X0, pers => X1, wh => (-)}
	    at np,
	     top=vp{num => X0, pers => X1, mode => X2}
	    and bot=vp{mode => X3, num => X6, pers => X7}
	    at vp(
		  bot=v{mode => X3, num => X6, pers => X7}
		 at <> v
		 )
	    )
	}.

tag_tree{ name => w0t,
	  family => t,
	  tree=> tree
	bot=s{inv => X1, quest => (+), mode => X3}
	at -s( np( [qui] ),
	       top=s{inv => X1}
	     and bot=s{inv => (-), mode => X3}
	     at s(
		  id=np_0
	     at -np( [] ),
		  top=vp{mode => X3, num => sing, pers => 3}
		 and bot=vp{mode => X4, num => X7, pers => X8}
		 at vp(
		       bot=v{mode => X4, num => X7, pers => X8}
		      at <> v
		      )
		 )
	     )
	}.

tag_tree{ name => r0t,
	  family => t,
	  tree=> auxtree np(
			    top=np{num => X2, pers => X3}
			   at *np,
			    -s(
			       np( [qui] ),
			       bot=s{mode => X0}
			      at s(
			id=np_0
				  at -np( [] ),
				   top=vp{mode => X0, num => X2, pers => X3}
				  and bot=vp{mode => X1, num => X4, pers => X5}
				  at vp(
					bot=v{mode => X1, num => X4, pers => X5}
				       at <> v
				       )
				  )
			      )
			   )
	}.

%% Family ta {ta,w0ta,r0ta,wata}

tag_tree{ name => ta,
	  family => ta,
	  tree=> tree
	bot=s{inv => (-), mode => X6}
	at s(
	     top=np{num => X0, pers => X1, wh => (-), gen => X9, restr => X10}
	    at np,
	     top=vp{num => X0, pers => X1, mode => X6}
	    and bot=vp{num => X4, pers => X5, mode => X7}
	    at vp(
		  bot=v{num => X4, pers => X5, mode => X7}
		 at <> v,
		  top=ap{num => X0, gen => X9, restr => X10}
		 at ap
	     )
	    )
	}.

tag_tree{ name => w0ta,
	  family => ta,
	  tree=> tree
	bot=s{inv => X1, quest => (+)}
	at s( np( [qui] ),
	      top=s{inv => X1}
	    and bot=s{inv => (-), mode => X3}
	    at s(
	     top=np{restr => X11}
		and bot=np{restr => X11}
		at -np( [] ),
		 top=vp{mode => X3, num => sing, pers => 3}
		and bot=vp{mode => X4, num => X7, pers => X8}
		at vp(
		      bot=v{mode => X4, num => X7, pers => X8}
		     at <> v,
		      top=ap{num => sing, gen => 3, restr => X11}
		     at ap
		     )
		)
	    )
	}.

tag_tree{ name => r0ta,
	  family => ta,
	  tree=> auxtree np(
			    top=np{num => X0, pers => X1, restr => X7, gen => X9}
			   at *np,
			    -s( np( [qui] ),
				bot=s{mode => X2, inv => (-)}
			      at s(
				   top=np{restr => X7}
				  at -np( [] ),
				   top=vp{num => X0, pers => X1, mode => X2}
				  and bot=vp{mode => X3, num => X5, pers => X6}
				  at vp(
					bot=v{mode => X3, num => X5, pers => X6}
				       at <> v,
					top=ap{num => X0, gen => X9, restr => X7}
				       at ap
				       )
				  )
			      )
			   )
	}.

tag_tree{ name => wata,
	  family => ta,
	  tree=> tree
	bot=s{inv => (-), quest => (+)}
	at s( ap( [comment] ),	% WARNING: non terminal comment or terminal comment ?
	      bot=s{mode => X5}
	    at s(
		 top= np{num => X0, pers => X1, wh => (-)}
		at np,
		 top=vp{num => X0, pers => X1, mode => X5}
		and bot=vp{num => X3, pers => X4, mode => X6}
		at vp(
		      bot=v{num => X3, pers => X4, mode => X6}
		     at <> v,
		      -ap( [] )
		     )
		)
	    )
	}.

%% Family tdn1 {tdn1,w0tdn1,r0tdn1}

tag_tree{ name => tdn1,
	  family => tdn1,
	  tree=> tree
	bot=s{mode => X2, inv => (-)}
	at s(
	     id=np_0
	and top=np{num => X0, pers => X1, wh => (-)}
	    at np,
	     top=vp{num => X0, pers => X1, mode => X2}
	    and bot=vp{mode => X3, num => X6, pers => X7}
	    at vp(
		  bot=v{mode => X3, num => X6, pers => X7}
		 at <> v,
		  np(
		     id=d_1 at d,
		     id=n_1 at n
		    )
		 )
	    )
	}.

tag_tree{ name => w0tdn1,
	  family => tdn1,
	  tree=> tree
	bot=s{inv => X1, quest => (+), mode => X3}
	at s( np( [qui] ),
	      top=s{inv => X1}
	    and bot=s{inv => (-), mode => X3}
	    at s(
		 id=np_0
		at -np( [] ),
		 top=vp{mode => X3, num => sing, pers => 3}
		and bot=vp{mode => X4, num => X7, pers => X8}
		at vp(
		      bot=v{mode => X4, num => X7, pers => X8}
		     at <> v,
		      np(
			 id=d_1 at d,
			 id=n_1 at n
			)
		     )
		)
	    )
	}.

tag_tree{ name => r0tdn1,
	  family => tdn1,
	  tree=> auxtree np(
			    top=np{num => X2, pers => X3}
			   at *np,
			    -s(
			       np( [qui] ),
			       bot=s{mode => X0}
			      at s(
				   id=np_0
				  at -np( [] ),
				   top=vp{mode => X0, num => X2, pers => X3}
				  and bot=vp{mode => X1, num => X4, pers => X5}
				  at vp(
					bot=v{mode => X1, num => X4, pers => X5}
				       at <> v,
					np(
					   id=d_1 at d,
					   id=n_1 at n
					  )
				       )
				  )
			      )
			   )
	}.


%% Family tn1 {tn1,w0tn1,r0tn1,r1tn1,w1tn1}

tag_tree{ name => tn1,
	  family => tn1,
	  tree=> tree
	bot=s{mode => X2, inv => (-)}
	at s(
	     id=np_0
	    and top=np{num => X0, pers => X1, wh => (-)}
	    at np,
	     top=vp{num => X0, pers => X1, mode => X2}
	    and bot=vp{mode => X3, num => X6, pers => X7}
	    at vp(
		  bot=v{mode => X3, num => X6, pers => X7}
		 at <> v,
		  id=np_1 at np
		 )
	    )
	}.

tag_tree{ name => w0tn1,
	  family => tn1,
	  tree=> tree
	bot=s{inv => X1, quest => (+)}
	at -s(
	      np( [qui] ),
	      top=s{inv => X1}
	     and bot=s{inv => (-), mode => X3}
	     at s(
		  id=np_0 at -np( [] ),
		  top=vp{mode => X3, num => sing, pers => 3}
		 and bot=vp{mode => X4, num => X7, pers => X8}
		 at vp(
		       bot=v{mode => X4, num => X7, pers => X8}
		      at <> v,
		       id=np_1
		      at np
		      )
		 )
	     )
	}.

tag_tree{ name => r0tn1,
	  family => tn1,
	  tree=> auxtree np(
			    top=np{num => X0, pers => X1, restr => X7}
			   at *np,
			    -s( np( [qui] ),
				bot=s{mode => X2, inv => (-)}
			      at s(
				   id=np_0
				  and top=np{restr => X7}
				  at -np( [] ),
				   top=vp{num => X0, pers => X1, mode => X2}
				  and bot=vp{mode => X3, num => X5, pers => X6}
				  at vp(
					bot=v{mode => X3, num => X5, pers => X6}
				       at <> v,
					id=np_1 at np
				       )
				  )
			      )
			   )
	}.

tag_tree{ name => r1tn1,
	  family => tn1,
	  tree=> auxtree np(
			    top=np{restr => X0}
			   at *np,
			    -s( np( [que] ),
				bot=s{mode => X3}
			      at s(
				   id=np_0
				  and top=np{num => X1, pers => X2, wh => (-)}
				  at np,
				   top=vp{num => X1, pers => X2, mode => X3}
				  and bot=vp{mode => X4, num => X6, pers => X7}
				  at vp(
					bot=v{mode => X4, num => X6, pers => X7}
				       at <> v,
					id=np_1
				       and top=np{restr => X0}
				       at -np( [] )
				       )
				  )
			      )
			   )
	}.

tag_tree{ name => w1tn1,
	  family => tn1,
	  tree=> tree
	bot=s{inv => (-), quest => (+)}
	at s(
	     top=np{restr => X0, wh => (+)}
	    at np,
	     bot=s{mode => X6}
	    at s(
	     id=np_0
		and top=np{num => X4, pers => X5, wh => (-)}
	    at np,
		 top=vp{num => X4, pers => X5, mode => X6}
		and bot=vp{mode => X7, num => X9, pers => X10}
		at vp(
		      bot=v{mode => X7, num => X9, pers => X10}
		     at <> v,
		      id=np_1
		     and top=np{restr => X0}
		     at -np( [] )
		     )
		)
	    )
	}.

%% Family tn1pn2 {tn1pn2,w1tn1pn2,w0tn1pn2,w2tn1pn2,r1tn1pn2,r2tn1pn2,r0tn1pn2}

tag_tree{ name => tn1pn2,
	  family => tn1pn2,
	  tree=> tree
	bot=s{mode => X2, inv => (-)}
	at s(
	     id=np_0
	    and top=np{num => X0, pers => X1, wh => (-)}
	    at np,
	     top=vp{num => X0, pers => X1, mode => X2}
	    and bot=vp{mode => X3, num => X6, pers => X7}
	    at vp(
		  bot=v{mode => X3, num => X6, pers => X7}
		 at <> v,
		  np,
		  pp(
		     id=p_2 at p,
		     id=np_2 at np
		    )
		 )
	    )
	}.

tag_tree{ name => w1tn1pn2,
	  family => tn1pn2,
	  tree=> tree s(
			np,
			bot=s{mode => X2, inv => (-)}
		       at s(
			    id=np_0
			   and top=np{num => X0, pers => X1, wh => (-)}
			   at np,
			    top=vp{num => X0, pers => X1, mode => X2}
			   and bot=vp{mode => X3, num => X6, pers => X7}
			   at vp(
				 bot=v{mode => X3, num => X6, pers => X7}
				at <> v,
				 -np( [] ),
				 pp(
				    id=p_2 at p,
				    id=np_2 at np
				   )
				)
			   )
		       )
	}.

tag_tree{ name => w0tn1pn2,
	  family => tn1pn2,
	  tree=> tree bot=s{inv => X1, quest => (+)}
	at s( np( [qui] ),
	      top=s{inv => X1}
	    and bot=s{inv => (-), mode => X3}
	    at s(
		 id=np_0
		at -np( [] ),
		 top=vp{mode => X3, num => sing, pers => 3}
		and bot=vp{mode => X4, num => X7, pers => X8}
		at vp(
		      bot=v{mode => X4, num => X7, pers => X8}
		     at <> v,
		      np,
		      pp(
			 id=p_2 at p,
			 id=np_2 at np
			)
		     )
		)
	    )
	}.

tag_tree{ name => w2tn1pn2,
	  family => tn1pn2,
	  tree=> tree bot=s{mode => X2, inv => (-)}
	at s(
	     pp(
		id=p_2 at p,
		id=np_2
	       and top=np{wh => (+)}
	       and bot=np{wh => (+)}
	       at np
	       ),
	     s(
	       id=np_0
	      and top=np{num => X0, pers => X1, wh => (-)}
	      at np,
	       top=vp{num => X0, pers => X1, mode => X2}
	      and bot=vp{mode => X3, num => X6, pers => X7}
	      at vp(
		    bot=v{mode => X3, num => X6, pers => X7}
		   at <> v,
		    np,
		    pp( [] )	% WARNING: adjonction allowed on trace of pp ?
		   )
	      )
	    )
	}.

tag_tree{ name => r1tn1pn2,
	  family => tn1pn2,
	  tree=> auxtree np(
			    top=np{restr => X0}
			   at *np,
			    -s( np( [que] ),
				bot=s{mode => X3}
			      at s(
				   id=np_0
				  and top=np{num => X1, pers => X2, wh => (-)}
				  at np,
				   top=vp{num => X1, pers => X2, mode => X3}
				  and bot=vp{mode => X4, num => X6, pers => X7}
				  at vp(
					bot=v{mode => X4, num => X6, pers => X7}
				       at <> v,
					top=np{restr => X0}
				       at -np( [] ),
					pp(
					   id=p_2 at p,
					   id=np_2 at np
					  )
				       )
				  )
			      )
			   )
	}.

tag_tree{ name => r2tn1pn2,
	  family => tn1pn2,
	  tree=> auxtree np(
			    *np,
			    -s(
			       pp(
				  id=p_2 at p,
				  id=np_2
				 and top=np{wh => (+)}
				 at np
				 ),
			       bot=s{mode => X2, inv => (-)}
			      at s(
				   id=np_0
				  and top=np{num => X0, pers => X1, wh => (-)}
				  at np,
				   vp(
				      top=v{num => X0, pers => X1, mode => X2}
				     and bot=v{mode => X3, num => X7, pers => X8 }
				     at <> v,
				      np,
				      pp( [] ) % WARNING: adjonction allowed on trace of pp ?
				     )
				  )
			      )
			   )
	}.

tag_tree{ name => r0tn1pn2,
	  family => tn1pn2,
	  tree=> auxtree np(
			    top=np{num => X0, pers => X1, restr => X7}
			   at *np,
			    -s( np( [qui] ),
				bot=s{mode => X2, inv => (-)}
			      at s(
				   id=np_0
				  and top=np{restr => X7}
				  at -np( [] ),
				   top=vp{num => X0, pers => X1, mode => X2}
				  and bot=vp{mode => X3, num => X5, pers => X6}
				  at vp(
					bot=v{mode => X3, num => X5, pers => X6}
				       at <> v,
					np,
					pp(
					   id=p_2 at p,
					   id=np_2 at np
					  )
				       )
				  )
			      )
			   )
	}.

%% Family tpdn1 {w0tpdn1,r0tpdn1,tpdn1}

tag_tree{ name => w0tpdn1,
	  family => tpdn1,
	  tree=> tree
	bot=s{inv => X1, quest => (+)}
	at -s(
	      np( [qui] ),
	      top=s{inv => X1}
	     and bot=s{inv => (-), mode => X3}
	     at s(
		  id=np_0 at -np( [] ),
		  top=vp{mode => X3, num => sing, pers => 3}
		 and bot=vp{mode => X4, num => X7, pers => X8}
		 at vp(
		       bot=v{mode => X4, num => X7, pers => X8}
		      at <> v,
		       pp(
			  id=p_1 at p,
			  np(
			     id=d_1 at d,
			     id=n_1 at n
			    )
			 )
		      )
		 )
	     )
	}.

tag_tree{ name => r0tpdn1,
	  family => tpdn1,
	  tree=> auxtree np(
			    top=np{num => X0, pers => X1, restr => X7}
			   at *np,
			    -s(
			       top=np{num => X0, pers => X1, restr => X7}
			      at np( [qui] ),
			       bot=s{mode => X2, inv => (-)}
			      at s(
				   id=np_0
				  and top=np{restr => X7}
				  at -np( [] ),
				   top=v{num => X0, pers => X1, mode => X2}
				  and bot=v{mode => X3, num => X5, pers => X6 }
				  at v(
				       top=v{num => X0, pers => X1, mode => X2}
				      and bot=v{mode => X3, num => X5, pers => X6}
				      at <> v,
				       pp(
					  id=p_1 at p,
					  np(
					     id=d_1 at d,
					     id=n_1 at n
					    )
					 )
				      )
				  )
			      )
			   )
	}.

tag_tree{ name => tpdn1,
	  family => tpdn1,
	  tree=> tree
	bot=s{mode => X2, inv => (-)}
	at s(
	     id=np_0
	    and top=np{num => X0, pers => X1, wh => (-)}
	    at np,
	     top=vp{num => X0, pers => X1, mode => X2}
	    and bot=vp{mode => X3, num => X6, pers => X7}
	    at vp(
		  bot=v{mode => X3, num => X6, pers => X7}
		 at <> v,
		  pp(
		     id=p_1 at p,
		     np(
			id=d_1 at d,
			id=n_1 at n
		       )
		    )
		 )
	    )
	}.

%% Family tpn1 {w0tpn1,w1tpn1,tpn1,r0tpn1,r1tpn1}

tag_tree{ name => w0tpn1,
	  family => tpn1,
	  tree=> tree
	bot=s{inv => X1, quest => (+)}
	at -s(
	      top=np{wh => (+)} at np,
	      top=s{inv => X1}
	     and bot=s{inv => (-), mode => X3}
	     at s(
		  id=np_0
		 at -np( [] ),
		  top=vp{mode => X3, num => sing, pers => 3}
		 and bot=vp{mode => X4, num => X7, pers => X8}
		 at vp(
		       bot=v{mode => X4, num => X7, pers => X8}
		      at <> v,
		       pp(
			  id=p_1 at p,
			  id=np_1 at np
			 )
		      )
		 )
	     )
	}.

tag_tree{ name => w1tpn1,
	  family => tpn1,
	  tree=> tree
	bot=s{inv => X0, quest => (+)}
	at -s(
	      pp(
		 id=p_1 at p,
		 id=np_1
		and top=np{wh => (+)}
		at np
		),
	      top=s{inv => X0}
	     and bot=s{mode => X5, inv => (-)}
	     at s(
		  top=v{num => X3, pers => X4, mode => X5}
		 and bot=v{mode => X6, num => X9, pers => X10}
		 at <> v,
		  id=np_0
		 and top=np{num => X3, pers => X4, wh => (-)}
		 at np,
		  pp( [] )		% WARNING: adjonction allowed on trace of pp ?
		 )
	     )
	}.

tag_tree{ name => tpn1,
	  family => tpn1,
	  tree=> tree
	bot=s{mode => X2, inv => (-)}
	at s(
	     id=np_0
	    and top=np{num => X0, pers => X1, wh => (-)}
	    at np,
	     vp(
		top=v{num => X0, pers => X1, mode => X2}
	       and bot=v{mode => X3, num => X6, pers => X7}
	       at <> v,
		pp(
		   id=p_1 at p,
		   id=np_1 at np
		  )
	       )
	    )
	}.

tag_tree{ name => r0tpn1,
	  family => tpn1,
	  tree=> auxtree np(
			    top=np{num => X0, pers => X1, restr => X7}
			   at *np,
			    -s(
			       np,
			       bot=s{mode => X2, inv => (-)}
			      at s(
				   id=np_0
				  and top=np{restr => X7}
				  at -np( [] ),
				   vp(
				      top=v{num => X0, pers => X1, mode => X2}
				     and bot=v{mode => X3, num => X5, pers => X6}
				     at <> v,
				      pp(
					 id=p_1 at p,
					 id=np_1 at np
					)
				     )
				  )
			      )
			   )
	}.

tag_tree{ name => r1tpn1,
	  family => tpn1,
	  tree=> auxtree np(
			    *np,
			    -s(
			       pp(
				  id=p_1 at p,
				  id=np_1
				 and top=np{wh => (+)}
				 at np
				 ),
			       bot=s{mode => X2, inv => (-)}
			      at s(
				   id=np_0
				  and top=np{num => X0, pers => X1, wh => (-)}
				  at np,
				   vp(
				      top=v{num => X0, pers => X1, mode => X2}
				     and bot=v{mode => X3, num => X7, pers => X8}
				     at <> v,
				      pp( [] ) % WARNING: adjonction allowed on trace of pp ?
				     )
				  )
			      )
			   )
	}.

%% Family ts1 {r0ts1,ts1,w0ts1,w1ts1}

tag_tree{ name => r0ts1,
	  family => ts1,
	  tree=> auxtree np(
			    top=np{num => X0, pers => X3}
			   at *np,
			    -s( np( [qui] ),
				top=s{inv => (-)}
			      and bot=s{inv => (-)}
			      at s(
				   id=np_0
				  at -np( [] ),
				   top=vp{num => X0, pers => X3}
				  and bot=vp{num => X1, pers => X2, mode => X4}
				  at vp(
					id=v_
				       and bot=v{num => X1, pers => X2, mode => X4}
				       at <> v,
					id=s_1
				       at s
				       )
				  )
			      )
			   )
	}.

tag_tree{ name => ts1,
	  family => ts1,
	  tree=> auxtree
	bot=s{mode => X2, comp => (-), inv => (-)}
	at s(
	     id=np_0
	    and top=np{num => X0, pers => X1, wh => (-)}
	    at np,
	     top=vp{num => X0, pers => X1, mode => X2}
	    and bot=vp{mode => X3, num => X4, pers => X5}
	    at vp(
		  id=v_
		 and bot=v{mode => X3, num => X4, pers => X5}
		 at <> v,
		  bot=s{comp => (+)}
		 at s(
		      comp( [que] ),
		      id=s_1
		     and top=s{inv => (-), comp => (-)}
		     at *s
		 )
		 )
	    )
	}.

tag_tree{ name => w0ts1,
	  family => ts1,
	  tree=> auxtree -s( np( [qui] ),
			     bot=s{mode => X4, inv => (-)}
			   at s(
				id=np_0
			       at -np( [] ),
				top=vp{mode => X4}
			       and bot=vp{num => sing, pers => 3}
			       at vp(
				     id=v_
				    and bot=v{num => sing, pers => 3}
				    at <> v,
				     -s( comp( [que] ),
					 id=s_1
				       at *s
				       )
				    )
			       )
			   )
	}.

tag_tree{ name => w1ts1,
	  family => ts1,
	  tree=> tree -s( np( [que] ),
			  bot=s{mode => X4, inv => (-)}
			at s(
			     top=vp{num => X0, pers => X1, mode => X4}
			    and bot=vp{num => X2, pers => X3, mode => X5}
			    at vp(
				  id=v_
				 and bot=v{num => X2, pers => X3, mode => X5}
				 at <> v,
				  id=np_0
				 and top=np{num => X0, pers => X1}
				 at np,
				  id=s_1
				 at -s( [] )
				 )
			    )
			)
	}.


tag_tree{ name => npcoord,
	  family => coord,
	  tree => auxtree np(id=coord1 at *np,
			     (	 ([','],np)  @*),
			     <>coord,
			     id=coord2 at np)
	}
.

tag_tree{ name => scoord,
	  family => coord,
	  tree => auxtree s(id=coord1 at *s,
			    (	([','],s)  @*),
			    <>coord, id=coord2 at s)
	}
.

tag_tree{ name => acoord,
	  family => coord,
	  tree => auxtree a(id=coord1 at *a,
			    (	([','],ap)  @*),
			    <>coord,
			    id=coord2 at ap)
	}
.

tag_tree{ name => ncoord,
	  family => coord,
	  tree => auxtree n(id=coord1 at *n,
			    (	([','],n)  @*),
			    <>coord,
			    id=coord2 at n)
	}
.

/* Not possible to use following tree because vp is not a standalone non-terminal

tag_tree{ name => vpcoord,
	  family => coord,
	  tree => auxtree vp(id=coord1 at *vp,<>coord,id=coord2 at vp)
	}
.
*/







