/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2000 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  xtag_header.tag -- header file for French XTAG
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */
:-features([ap],[gen,num,restr]).
:-features([np],[gen,num,pers,restr,wh]).
:-features([a],[gen,num,restr]).
:-features([d],[gen,npers,num,pers,poss,wh]).
:-features([vp],[gen,mode,num,pers]).
:-features([n],[gen,num,restr]).
:-features([s],[comp,inv,mode,quest]).
:-features([v],[gen,mode,num,pers,tense]).

:-finite_set( mode, [ind,subj,inf] ).
:-finite_set( tense, [pres,fut]).
:-finite_set( num, [pl,sing] ).
:-finite_set( gender, [fem,masc] ).
:-finite_set( pers, [1,2,3] ).
:-finite_set( restr, [moinshum,plushum] ).
:-finite_set( [wh,inv,poss], [+,-] ).

:-features( lemma, [lex,lemma,cat,top,anchor] ).
:-features( tag_anchor, [name, coanchors, equations ] ).
:-op(710,xfx,[at]).
:-op(705,xfy,[and]).
