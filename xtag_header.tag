/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2000, 2003, 2010 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  xtag_header.tag -- header file for French XTAG
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-require 'tag_generic.pl'.

:-features( tag_tree, [ family, name, tree ] ).
:-features( tag_anchor, [name, coanchors, equations ] ).

:-finite_set( mode, [ind,subj,inf] ).
:-finite_set( tense, [pres,fut]).
:-finite_set( num, [pl,sing] ).
:-finite_set( gender, [fem,masc] ).
:-finite_set( pers, [1,2,3] ).
:-finite_set( restr, [moinshum,plushum] ).
:-finite_set( [wh,inv,poss], [+,-] ).
	      
:-features([ap],[gen,num,restr]).
:-features([np],[gen,num,pers,restr,wh]).
:-features([a],[gen,num,restr]).
:-features([d],[gen,npers,num,pers,poss,wh]).
:-features([vp],[gen,mode,num,pers]).
:-features([n],[gen,num,restr]).
:-features([s],[comp,inv,mode,quest]).
:-features([v],[gen,mode,num,pers,tense]).

:-tag_features(ap,ap{},ap{}).
:-tag_features(np,np{},np{}).
:-tag_features(a,a{},a{}).
:-tag_features(d,d{},d{}).
:-tag_features(vp,vp{},vp{}).
:-tag_features(n,n{},n{}).
:-tag_features(s,s{},s{}).
:-tag_features(v,v{},v{}).

:-tag_mode(_,all,[+|-],+,-).

%%:-debug.

:-include 'xtag_anchors.tag'.

