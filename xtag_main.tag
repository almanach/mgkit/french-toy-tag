/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2000, 2004 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  xtag_main.tag -- French Toy XTAG (non lexicalized)
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-include 'xtag_header.tag'.

:-require
	'xtag_trees.tag'
	.
	   

?-argv(L), ( L=[] -> recorded('N'(N)), A=0, tag_phrase(top=S::s{} at -s,A,N)
	   ;   L=['-robust'] -> 
	       wait(analyze(_,_,_)),
	       extract_best_solution(A,N,U),
	       analyze(A,N,U)
	   ;
	       fail
	   )
.

analyze(A,N,U) :- tag_phrase(top=U::s{} at -s,A,N), N > A+1.
analyze(A,N,U) :- tag_phrase(top=U::np{} at -np,A,N), N > A+1.

:-std_prolog extract_analyze/3.

extract_analyze(A,N,U) :-
	item_term(I,analyze(A,N,U)),
	recorded(I,_)
	.

:-std_prolog extract_best_solution/3.

extract_best_solution(L,R,U) :-
	extract_analyze(L,R,U),
	\+ ( extract_analyze(L2,R2,_),
	       L2 =< L, R =< R2,
	       \+ (L=L2, R=R2)
	   )
	.
