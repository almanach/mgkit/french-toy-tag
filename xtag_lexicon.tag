/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2000, 2004 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  xtag_lexicon.tag -- Lexicon for French Toy XTAG
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-include 'xtag_header.tag'.

tag_lexicon(aime, '*AIMER*', v, v{ mode => mode[ind, subj], num => sing }).
tag_lexicon(aiment, '*AIMER*', v, v{ mode => mode[ind, subj], num => pl }).
tag_lexicon(aimera, '*AIMER*', v, v{ mode => ind, num => sing, tense => fut }).
tag_lexicon(aimeront, '*AIMER*', v, v{ mode => ind, num => pl, tense => fut }).
tag_lexicon(aimer, '*AIMER*', v, v{ mode => inf }).
tag_lexicon(a, '*AVOIR*', v, v{ mode => ind, num => sing, tense => pres }).
tag_lexicon(ait, '*AVOIR*', v, v{ mode => subj, num => sing }).
tag_lexicon(ont, '*AVOIR*', v, v{ mode => ind, num => pl, tense => pres }).
tag_lexicon(aient, '*AVOIR*', v, v{ mode => subj, num => pl }).
tag_lexicon(avoir, '*AVOIR*', v, v{ mode => inf }).
tag_lexicon(aura, '*AVOIR*', v, v{ mode => ind, num => sing, tense => fut }).
tag_lexicon(auront, '*AVOIR*', v, v{ mode => ind, num => pl, tense => fut }).
tag_lexicon(aller, '*ALLER*', v, v{ mode => inf }).
tag_lexicon(va, '*ALLER*', v, v{ mode => ind, num => sing }).
tag_lexicon(vont, '*ALLER*', v, v{ mode => ind, num => pl }).
tag_lexicon(aille, '*ALLER*', v, v{ mode => subj, num => sing }).
tag_lexicon(aillent, '*ALLER*', v, v{ mode => subj, num => pl }).
tag_lexicon(ira, '*ALLER*', v, v{ mode => ind, num => sing, tense => fut }).
tag_lexicon(iront, '*ALLER*', v, v{ mode => ind, num => pl, tense => fut }).
tag_lexicon(am�ricain, '*AM�RICAIN*', a, a{ gen => masc, num => sing }).
tag_lexicon(am�ricaine, '*AM�RICAIN*', a, a{ gen => fem, num => sing }).
tag_lexicon(am�ricains, '*AM�RICAIN*', a, a{ gen => masc, num => pl }).
tag_lexicon(am�ricaines, '*AM�RICAIN*', a, a{ gen => fem, num => pl }).
tag_lexicon(ami, '*AMI*', n, n{ gen => masc, num => sing }).
tag_lexicon(amie, '*AMI*', n, n{ gen => fem, num => sing }).
tag_lexicon(amies, '*AMI*', n, n{ gen => fem, num => pl }).
tag_lexicon(amis, '*AMI*', n, n{ gen => masc, num => pl }).
tag_lexicon(bleue, '*BLEU*', a, a{ gen => fem, num => sing }).
tag_lexicon(bleu, '*BLEU*', a, a{ gen => masc, num => sing }).
tag_lexicon(bleus, '*BLEU*', a, a{ gen => masc, num => pl }).
tag_lexicon(bleues, '*BLEU*', a, a{ gen => fem, num => pl }).
tag_lexicon(beau, '*BEAU*', a, a{ gen => masc, num => sing }).
tag_lexicon(belle, '*BEAU*', a, a{ gen => fem, num => sing }).
tag_lexicon(beaux, '*BEAU*', a, a{ gen => masc, num => pl }).
tag_lexicon(belles, '*BEAU*', a, a{ gen => fem, num => pl }).
tag_lexicon(b�te, '*BETE*', a, a{ num => sing }).
tag_lexicon(b�tes, '*BETE*', a, a{ num => pl }).
tag_lexicon(boit, '*BOIRE*', v, v{ mode => ind, num => sing }).
tag_lexicon(boivent, '*BOIRE*', v, v{ mode => mode[ind, subj], num => pl }).
tag_lexicon(boire, '*BOIRE*', v, v{ mode => inf }).
tag_lexicon(boive, '*BOIRE*', v, v{ mode => subj, num => sing }).
tag_lexicon(boira, '*BOIRE*', v, v{ mode => ind, num => sing, tense => fut }).
tag_lexicon(boiront, '*BOIRE*', v, v{ mode => ind, num => pl, tense => fut }).
tag_lexicon(bon, '*BON*', a, a{ gen => masc, num => sing }).
tag_lexicon(bonne, '*BON*', a, a{ num => sing, gen => fem }).
tag_lexicon(bons, '*BON*', a, a{ gen => masc, num => pl }).
tag_lexicon(bonnes, '*BON*', a, a{ num => pl, gen => fem }).
tag_lexicon(brise, '*BRISER*', v, v{ mode => mode[ind, subj], num => sing }).
tag_lexicon(briser, '*BRISER*', v, v{ mode => inf }).
tag_lexicon(brisent, '*BRISER*', v, v{ mode => mode[ind, subj], num => pl }).
tag_lexicon(brisera, '*BRISER*', v, v{ mode => ind, num => sing, tense => fut }).
tag_lexicon(briseront, '*BRISER*', v, v{ mode => ind, num => pl, tense => fut }).
tag_lexicon(ce, '*CE*', d, d{ num => sing, gen => masc }).
tag_lexicon(cette, '*CE*', d, d{ num => sing, gen => fem }).
tag_lexicon(cet, '*CE*', d, d{ num => sing, gen => masc }).
tag_lexicon(ces, '*CE*', d, d{ num => pl }).
tag_lexicon(chat, '*CHAT*', n, n{ gen => masc, num => sing }).
tag_lexicon(chats, '*CHAT*', n, n{ gen => masc, num => pl }).
tag_lexicon(chattes, '*CHAT*', n, n{ gen => fem, num => pl }).
tag_lexicon(chatte, '*CHAT*', n, n{ gen => fem, num => sing }).
tag_lexicon(cherchent, '*CHERCHER*', v, v{ mode => mode[ind, subj], num => pl }).
tag_lexicon(cherche, '*CHERCHER*', v, v{ mode => mode[ind, subj], num => sing }).
tag_lexicon(chercher, '*CHERCHER*', v, v{ mode => inf }).
tag_lexicon(cherchera, '*CHERCHER*', v, v{ mode => ind, num => sing, tense => fut }).
tag_lexicon(chercheront, '*CHERCHER*', v, v{ mode => ind, num => pl, tense => fut }).
tag_lexicon(chien, '*CHIEN*', n, n{ gen => masc, num => sing }).
tag_lexicon(chiens, '*CHIEN*', n, n{ gen => masc, num => pl }).
tag_lexicon(chienne, '*CHIEN*', n, n{ gen => fem, num => sing }).
tag_lexicon(chiennes, '*CHIEN*', n, n{ gen => fem, num => pl }).
tag_lexicon(comprend, '*COMPRENDRE*', v, v{ num => sing, mode => ind }).
tag_lexicon(comprenne, '*COMPRENDRE*', v, v{ num => sing, mode => subj }).
tag_lexicon(comprennent, '*COMPRENDRE*', v, v{ mode => mode[ind, subj], num => pl }).
tag_lexicon(comprendre, '*COMPRENDRE*', v, v{ mode => inf }).
tag_lexicon(comprendra, '*COMPRENDRE*', v, v{ mode => ind, num => sing, tense => fut }).
tag_lexicon(comprendront, '*COMPRENDRE*', v, v{ mode => ind, num => pl, tense => fut }).
tag_lexicon(courir, '*COURIR*', v, v{ mode => inf }).
tag_lexicon(court, '*COURIR*', v, v{ num => sing, mode => ind }).
tag_lexicon(courrra, '*COURIR*', v, v{ num => sing, mode => ind, tense => fut }).
tag_lexicon(courrent, '*COURIR*', v, v{ mode => mode[ind, subj], num => pl }).
tag_lexicon(courront, '*COURIR*', v, v{ mode => ind, num => pl, tense => fut }).
tag_lexicon(croit, '*CROIRE*', v, v{ num => sing, mode => ind }).
tag_lexicon(croire, '*CROIRE*', v, v{ mode => inf }).
tag_lexicon(croient, '*CROIRE*', v, v{ mode => mode[ind, subj], num => pl }).
tag_lexicon(croira, '*CROIRE*', v, v{ mode => ind, num => sing, tense => fut }).
tag_lexicon(croiront, '*CROIRE*', v, v{ mode => ind, num => pl, tense => fut }).
tag_lexicon(disent, '*DIRE*', v, v{ mode => mode[ind, subj], num => pl }).
tag_lexicon(dise, '*DIRE*', v, v{ mode => subj, num => sing }).
tag_lexicon(dire, '*DIRE*', v, v{ mode => inf }).
tag_lexicon(dit, '*DIRE*', v, v{ mode => ind, num => sing }).
tag_lexicon(dira, '*DIRE*', v, v{ mode => ind, num => sing, tense => fut }).
tag_lexicon(diront, '*DIRE*', v, v{ mode => ind, num => pl, tense => fut }).
tag_lexicon(doit, '*DEVOIR*', v, v{ mode => ind, num => sing }).
tag_lexicon(devoir, '*DEVOIR*', v, v{ mode => inf }).
tag_lexicon(doivent, '*DEVOIR*', v, v{ mode => mode[ind, subj], num => pl }).
tag_lexicon(doive, '*DEVOIR*', v, v{ mode => subj, num => sing }).
tag_lexicon(devra, '*DEVOIR*', v, v{ mode => ind, num => sing, tense => fut }).
tag_lexicon(devront, '*DEVOIR*', v, v{ mode => ind, num => pl, tense => fut }).
tag_lexicon(donne, '*DONNER*', v, v{ mode => mode[ind, subj], num => sing }).
tag_lexicon(donnent, '*DONNER*', v, v{ mode => mode[ind, subj], num => pl }).
tag_lexicon(donner, '*DONNER*', v, v{ mode => inf }).
tag_lexicon(donnera, '*DONNER*', v, v{ mode => ind, num => sing, tense => fut }).
tag_lexicon(donneront, '*DONNER*', v, v{ mode => ind, num => pl, tense => fut }).
tag_lexicon(dort, '*DORMIR*', v, v{ mode => ind, num => sing }).
tag_lexicon(dorme, '*DORMIR*', v, v{ mode => subj, num => sing }).
tag_lexicon(dorment, '*DORMIR*', v, v{ mode => mode[ind, subj], num => pl }).
tag_lexicon(dormir, '*DORMIR*', v, v{ mode => inf }).
tag_lexicon(dormiront, '*DORMIR*', v, v{ mode => ind, num => pl, tense => fut }).
tag_lexicon(dormira, '*DORMIR*', v, v{ mode => ind, num => sing, tense => fut }).
tag_lexicon(du, '*DU*', d, d{ num => sing, gen => masc }).
tag_lexicon(des, '*DU*', d, d{ num => pl }).
tag_lexicon(elle, '*ELLE*', np, np{ num=> sing, gen => fem }).
tag_lexicon(elles, '*ELLE*', np, np{ num=> pl, gen => fem }).
tag_lexicon(enfant, '*ENFANT*', n, n{ num => sing }).
tag_lexicon(enfants, '*ENFANT*', n, n{ num => pl }).
tag_lexicon(esp�re, '*ESP�RER*', v, v{ mode => mode[ind, subj], num => sing }).
tag_lexicon(esp�rer, '*ESP�RER*', v, v{ mode => inf }).
tag_lexicon(esp�rent, '*ESP�RER*', v, v{ mode => mode[ind, subj], num => pl }).
tag_lexicon(esp�rera, '*ESP�RER*', v, v{ mode => ind, num => sing, tense => fut }).
tag_lexicon(esp�reront, '*ESP�RER*', v, v{ mode => ind, num => pl, tense => fut }).
tag_lexicon(est, '*ETRE*', v, v{ mode => ind, num => sing }).
tag_lexicon(sont, '*ETRE*', v, v{ num => pl, mode => ind }).
tag_lexicon(sera, '*ETRE*', v, v{ mode => ind, num => sing, tense => fut }).
tag_lexicon(seront, '*ETRE*', v, v{ mode => ind, num => pl, tense => fut }).
tag_lexicon(soit, '*ETRE*', v, v{ mode => subj, num => sing }).
tag_lexicon(soient, '*ETRE*', v, v{ mode => subj, num => pl }).
tag_lexicon(fait, '*FAIRE*', v, v{ mode => ind, num => sing }).
tag_lexicon(fasse, '*FAIRE*', v, v{ mode => subj, num => sing }).
tag_lexicon(fassent, '*FAIRE*', v, v{ mode => subj, num => pl }).
tag_lexicon(faire, '*FAIRE*', v, v{ mode => inf }).
tag_lexicon(fera, '*FAIRE*', v, v{ mode => ind, num => sing, tense => fut }).
tag_lexicon(feront, '*FAIRE*', v, v{ mode => ind, num => pl, tense => fut }).
tag_lexicon(font, '*FAIRE*', v, v{ mode => ind, num => pl }).
tag_lexicon(faut, '*FALLOIR*', v, v{ mode => ind, num => sing }).
tag_lexicon(falloir, '*FALLOIR*', v, v{ mode => inf }).
tag_lexicon(faudra, '*FALLOIR*', v, v{ mode => ind, num => sing, tense => fut }).
tag_lexicon(femme, '*FEMME*', n, n{ num => sing, gen => fem }).
tag_lexicon(femmes, '*FEMME*', n, n{ num => pl, gen => fem }).
tag_lexicon(fille, '*FILLE*', n, n{ num => sing, gen => fem }).
tag_lexicon(filles, '*FILLE*', n, n{ num => pl, gen => fem }).
tag_lexicon(fleur, '*FLEUR*', n, n{ num => sing, gen => fem }).
tag_lexicon(fleurs, '*FLEUR*', n, n{ num => pl, gen => fem }).
tag_lexicon(garcon, '*GARCON*', n, n{ num => sing, gen => masc }).
tag_lexicon(garcons, '*GARCON*', n, n{ num => pl, gen => masc }).
tag_lexicon(grand, '*GRAND*', a, a{ gen => masc, num => sing }).
tag_lexicon(grande, '*GRAND*', a, a{ num => sing, gen => fem }).
tag_lexicon(grands, '*GRAND*', a, a{ gen => masc, num => pl }).
tag_lexicon(grandes, '*GRAND*', a, a{ gen => fem, num => pl }).
tag_lexicon(homme, '*HOMME*', n, n{ gen => masc, num => sing }).
tag_lexicon(hommes, '*HOMME*', n, n{ gen => masc, num => pl }).
tag_lexicon(intelligent, '*INTELLIGENT*', a, a{ gen => masc, num => sing }).
tag_lexicon(intelligents, '*INTELLIGENT*', a, a{ gen => masc, num => pl }).
tag_lexicon(intelligente, '*INTELLIGENT*', a, a{ num => sing, gen => fem }).
tag_lexicon(intelligentes, '*INTELLIGENT*', a, a{ gen => fem, num => pl }).
/*
tag_lexicon(il, '*IL*', n, n{ gen => masc, pers => 3, num => sing }).
tag_lexicon(ils, '*IL*', n, n{ gen => masc, pers => 3, num => pl }).
tag_lexicon(elle, '*IL*', n, n{ gen => sing, pers => 3, num => sing }).
tag_lexicon(elles, '*IL*', n, n{ gen => sing, pers => 3, num => pl }).
*/
tag_lexicon(joli, '*JOLI*', a, a{ gen => masc, num => sing }).
tag_lexicon(jolie, '*JOLI*', a, a{ gen => fem, num => sing }).
tag_lexicon(jolies, '*JOLI*', a, a{ gen => fem, num => pl }).
tag_lexicon(jouer, '*JOUER*', v, v{ mode => inf }).
tag_lexicon(joue, '*JOUER*', v, v{ mode => mode[ind, subj], num => sing }).
tag_lexicon(jouera, '*JOUER*', v, v{ mode => ind, num => sing, tense => fut }).
tag_lexicon(joueront, '*JOUER*', v, v{ mode => ind, num => pl, tense => fut }).
tag_lexicon(jouent, '*JOUER*', v, v{ mode => mode[ind, subj], num => pl }).
tag_lexicon(le, '*LE*', d, d{ gen => masc, num => sing }).
tag_lexicon('l''', '*LE*', d, d{ gen => masc, num => sing }).
tag_lexicon(la, '*LE*', d, d{ gen => fem }).
tag_lexicon(les, '*LE*', d, d{ num => pl }).
tag_lexicon(leur, '*LEUR*', d, d{ num => sing, pers => 3, npers => pl }).
tag_lexicon(leurs, '*LEUR*', d, d{ num => sing, pers => 3, npers => pl }).
tag_lexicon(lit, '*LIRE*', v, v{ mode => ind, num => sing }).
tag_lexicon(lire, '*LIRE*', v, v{ mode => inf }).
tag_lexicon(lisent, '*LIRE*', v, v{ mode => mode[subj, ind], num => pl }).
tag_lexicon(lise, '*LIRE*', v, v{ mode => subj, num => sing }).
tag_lexicon(lira, '*LIRE*', v, v{ mode => ind, num => sing, tense => fut }).
tag_lexicon(liront, '*LIRE*', v, v{ mode => ind, num => pl, tense => fut }).
tag_lexicon(livre, '*LIVRE*', n, n{ gen => masc, num => sing }).
tag_lexicon(livres, '*LIVRE*', n, n{ gen => masc, num => pl }).
tag_lexicon(mange, '*MANGER*', v, v{ mode => mode[ind, subj], num => sing }).
tag_lexicon(mangent, '*MANGER*', v, v{ mode => mode[ind, subj], num => pl }).
tag_lexicon(manger, '*MANGER*', v, v{ mode => inf }).
tag_lexicon(mangera, '*MANGER*', v, v{ mode => ind, num => sing, tense => fut }).
tag_lexicon(mangeront, '*MANGER*', v, v{ mode => ind, num => pl, tense => fut }).
tag_lexicon(m�re, '*MERE*', n, n{ num => sing, gen => fem }).
tag_lexicon(m�res, '*MERE*', n, n{ num => pl, gen => fem }).
tag_lexicon(met, '*METTRE*', v, v{ mode => ind, num => sing }).
tag_lexicon(mettent, '*METTRE*', v, v{ mode => mode[ind, subj], num => pl }).
tag_lexicon(mettre, '*METTRE*', v, v{ mode => inf }).
tag_lexicon(mettra, '*METTRE*', v, v{ mode => ind, num => sing, tense => fut }).
tag_lexicon(mettront, '*METTRE*', v, v{ mode => ind, num => pl, tense => fut }).
tag_lexicon(mette, '*METTRE*', v, v{ mode => subj, num => sing }).
tag_lexicon(montre, '*MONTRE*', n, n{ num => sing, gen => fem }).
tag_lexicon(montres, '*MONTRE*', n, n{ num => pl, gen => fem }).
tag_lexicon(montre, '*MONTRER*', v, v{ mode => mode[ind, subj], num => sing }).
tag_lexicon(montrent, '*MONTRER*', v, v{ mode => mode[ind, subj], num => pl }).
tag_lexicon(montrer, '*MONTRER*', v, v{ mode => inf }).
tag_lexicon(montrera, '*MONTRER*', v, v{ mode => ind, num => sing, tense => fut }).
tag_lexicon(montreront, '*MONTRER*', v, v{ mode => ind, num => pl, tense => fut }).
tag_lexicon(parfum, '*PARFUM*', n, n{ gen => masc, num => sing }).
tag_lexicon(parfums, '*PARFUM*', n, n{ gen => masc, num => pl }).
tag_lexicon(parler, '*PARLER*', v, v{ mode => inf }).
tag_lexicon(parle, '*PARLER*', v, v{ mode => mode[ind, subj], num => sing }).
tag_lexicon(parlent, '*PARLER*', v, v{ mode => mode[ind, subj], num => pl }).
tag_lexicon(parlera, '*PARLER*', v, v{ mode => ind, num => sing, tense => fut }).
tag_lexicon(parleront, '*PARLER*', v, v{ mode => ind, num => pl, tense => fut }).
tag_lexicon(partir, '*PARTIR*', v, v{ mode => inf }).
tag_lexicon(part, '*PARTIR*', v, v{ mode => ind, num => sing }).
tag_lexicon(partira, '*PARTIR*', v, v{ mode => ind, num => sing, tense => fut }).
tag_lexicon(partiront, '*PARTIR*', v, v{ mode => ind, num => pl, tense => fut }).
tag_lexicon(partent, '*PARTIR*', v, v{ mode => mode[ind, subj], num => pl }).
tag_lexicon(parte, '*PARTIR*', v, v{ mode => subj, num => sing }).
tag_lexicon(p�dale, '*PEDALE*', n, n{ num => sing, gen => fem }).
tag_lexicon(p�dales, '*PEDALE*', n, n{ num => pl, gen => fem }).
tag_lexicon(pense, '*PENSER*', v, v{ mode => mode[ind, subj], num => sing }).
tag_lexicon(pensent, '*PENSER*', v, v{ mode => mode[ind, subj], num => pl }).
tag_lexicon(penser, '*PENSER*', v, v{ mode => inf }).
tag_lexicon(pensera, '*PENSER*', v, v{ mode => ind, num => sing, tense => fut }).
tag_lexicon(penseront, '*PENSER*', v, v{ mode => ind, num => pl, tense => fut }).
tag_lexicon(perdre, '*PERDRE*', v, v{ mode => inf }).
tag_lexicon(perde, '*PERDRE*', v, v{ mode => subj, num => sing }).
tag_lexicon(perdent, '*PERDRE*', v, v{ mode => mode[ind, subj], num => pl }).
tag_lexicon(perd, '*PERDRE*', v, v{ mode => ind, num => sing }).
tag_lexicon(perdra, '*PERDRE*', v, v{ mode => ind, num => sing, tense => fut }).
tag_lexicon(perdront, '*PERDRE*', v, v{ mode => ind, num => pl, tense => fut }).
tag_lexicon(petit, '*PETIT*', a, a{ gen => masc, num => sing }).
tag_lexicon(petite, '*PETIT*', a, a{ gen => fem, num => sing }).
tag_lexicon(petits, '*PETIT*', a, a{ gen => masc, num => pl }).
tag_lexicon(petites, '*PETIT*', a, a{ gen => fem, num => pl }).
tag_lexicon(pleut, '*PLEUVOIR*', v, v{ mode => ind, num => sing }).
tag_lexicon(pleuvoir, '*PLEUVOIR*', v, v{ mode => inf }).
tag_lexicon(pleuvra, '*PLEUVOIR*', v, v{ mode => ind, num => sing, tense => fut }).
tag_lexicon(pleuve, '*PLEUVOIR*', v, v{ mode => subj, num => sing }).
tag_lexicon(peut, '*POUVOIR*', v, v{ mode => ind, num => sing }).
tag_lexicon(pouvoir, '*POUVOIR*', v, v{ mode => inf }).
tag_lexicon(peuvent, '*POUVOIR*', v, v{ mode => ind, num => pl }).
tag_lexicon(puisse, '*POUVOIR*', v, v{ mode => subj, num => sing }).
tag_lexicon(pourra, '*POUVOIR*', v, v{ mode => ind, num => sing, tense => fut }).
tag_lexicon(pourront, '*POUVOIR*', v, v{ mode => ind, num => pl, tense => fut }).
tag_lexicon(puissent, '*POUVOIR*', v, v{ mode => subj, num => pl }).
tag_lexicon(quel, '*QUEL*', d, d{ num => sing, gen => masc }).
tag_lexicon(quels, '*QUEL*', d, d{ num => pl, gen => masc }).
tag_lexicon(quelles, '*QUEL*', d, d{ num => pl, gen => fem }).
tag_lexicon(quelle, '*QUEL*', d, d{ num => sing, gen => fem }).
tag_lexicon(regarde, '*REGARDER*', v, v{ mode => mode[ind, subj], num => sing }).
tag_lexicon(regardent, '*REGARDER*', v, v{ mode => mode[ind, subj], num => pl }).
tag_lexicon(regarder, '*REGARDER*', v, v{ mode => inf }).
tag_lexicon(regardera, '*REGARDER*', v, v{ mode => ind, num => sing, tense => fut }).
tag_lexicon(regarderont, '*REGARDER*', v, v{ mode => ind, num => pl, tense => fut }).
tag_lexicon(rouge, '*ROUGE*', a, a{ num => sing }).
tag_lexicon(rouges, '*ROUGE*', a, a{ num => pl }).
tag_lexicon(sait, '*SAVOIR*', v, v{ mode => ind, num => sing }).
tag_lexicon(savent, '*SAVOIR*', v, v{ mode => ind, num => pl }).
tag_lexicon(savoir, '*SAVOIR*', v, v{ mode => inf }).
tag_lexicon(sache, '*SAVOIR*', v, v{ mode => subj, num => sing }).
tag_lexicon(sachent, '*SAVOIR*', v, v{ mode => subj, num => pl }).
tag_lexicon(saura, '*SAVOIR*', v, v{ mode => ind, num => sing, tense => fut }).
tag_lexicon(sauront, '*SAVOIR*', v, v{ mode => ind, num => pl, tense => fut }).
tag_lexicon(sauter, '*SAUTER*', v, v{ mode => inf }).
tag_lexicon(semblera, '*SEMBLER*', v, v{ mode => ind, num => sing, tense => fut }).
tag_lexicon(sembleront, '*SEMBLER*', v, v{ mode => ind, num => pl, tense => fut }).
tag_lexicon(semble, '*SEMBLER*', v, v{ mode => mode[ind, subj], num => sing }).
tag_lexicon(sembler, '*SEMBLER*', v, v{ mode => inf }).
tag_lexicon(semblent, '*SEMBLER*', v, v{ mode => mode[ind, subj], num => pl }).
tag_lexicon(son, '*SON*', d, d{ pers => 3, npers => sing, num => sing, gen => masc }).
tag_lexicon(sa, '*SON*', d, d{ pers => 3, npers => sing, gen => fem, num => sing }).
tag_lexicon(ses, '*SON*', d, d{ pers => 3, npers => sing, num => pl }).
tag_lexicon(table, '*TABLE*', n, n{ num => sing, gen => fem }).
tag_lexicon(tables, '*TABLE*', n, n{ num => pl, gen => fem }).
tag_lexicon(travaille, '*TRAVAILLER*', v, v{ mode => mode[ind, subj], num => sing }).
tag_lexicon(travaillent, '*TRAVAILLER*', v, v{ mode => mode[ind, subj], num => pl }).
tag_lexicon(travailler, '*TRAVAILLER*', v, v{ mode => inf }).
tag_lexicon(travaillera, '*TRAVAILLER*', v, v{ mode => ind, num => sing, tense => fut }).
tag_lexicon(travailleront, '*TRAVAILLER*', v, v{ mode => ind, num => pl, tense => fut }).
tag_lexicon(un, '*UN*', d, d{ gen => masc, num => sing }).
tag_lexicon(une, '*UN*', d, d{ gen => fem, num => sing }).
tag_lexicon(des, '*UN*', d, d{ num => pl }).
tag_lexicon(vendra, '*VENDRE*', v, v{ mode => ind, num => sing, tense => fut }).
tag_lexicon(vendront, '*VENDRE*', v, v{ mode => ind, num => pl, tense => fut }).
tag_lexicon(vend, '*VENDRE*', v, v{ mode => ind, num => sing }).
tag_lexicon(vendre, '*VENDRE*', v, v{ mode => inf }).
tag_lexicon(vende, '*VENDRE*', v, v{ mode => subj, num => sing }).
tag_lexicon(vendent, '*VENDRE*', v, v{ mode => mode[ind, subj], num => pl }).
tag_lexicon(vin, '*VIN*', n, n{ num => sing, gen => masc }).
tag_lexicon(vins, '*VIN*', n, n{ num => pl, gen => masc }).
tag_lexicon(visite, '*VISITER*', v, v{ mode => mode[ind, subj], num => sing }).
tag_lexicon(visitent, '*VISITER*', v, v{ mode => mode[ind, subj], num => pl }).
tag_lexicon(visiter, '*VISITER*', v, v{ mode => inf }).
tag_lexicon(visitera, '*VISITER*', v, v{ mode => ind, num => sing, tense => fut }).
tag_lexicon(visiteront, '*VISITER*', v, v{ mode => ind, num => pl, tense => fut }).
tag_lexicon(voit, '*VOIR*', v, v{ mode => ind, num => sing }).
tag_lexicon(voient, '*VOIR*', v, v{ mode => ind, num => pl }).
tag_lexicon(voir, '*VOIR*', v, v{ mode => inf }).
tag_lexicon(verra, '*VOIR*', v, v{ mode => ind, num => sing, tense => fut }).
tag_lexicon(verront, '*VOIR*', v, v{ mode => ind, num => pl, tense => fut }).
tag_lexicon(voie, '*VOIR*', v, v{ mode => subj, num => sing }).
tag_lexicon(vrai, '*VRAI*', a, a{ gen => masc, num => sing }).
tag_lexicon(vraie, '*VRAI*', a, a{ gen => fem, num => sing }).
tag_lexicon(vrais, '*VRAI*', a, a{ gen => masc, num => pl }).
tag_lexicon(vraies, '*VRAI*', a, a{ gen => fem, num => pl }).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Invariant forms

tag_lexicon('Yves','Yves',np,_).
tag_lexicon(vraiment,vraiment,ad,_).
tag_lexicon(vite,vite,ad,_).
tag_lexicon(vers,vers,p,_).
tag_lexicon(tard,tard,ad,_).
tag_lexicon(sur,sur,p,_).
tag_lexicon('St�phane','St�phane',np,_).
tag_lexicon(souvent,souvent,ad,_).
tag_lexicon(sous,sous,p,_).
tag_lexicon('Sharon','Sharon',np,_).
tag_lexicon('Sabine','Sabine',np,_).
tag_lexicon(que,que,np,_).
tag_lexicon(qui,qui,np,_).
tag_lexicon(quoi,quoi,np,_).
tag_lexicon(pour,pour,p,_).
tag_lexicon('Paris','Paris',np,_).
tag_lexicon(parfois,parfois,ad,_).
tag_lexicon(ou,ou,ad,_).
tag_lexicon(on,on,np,_).
tag_lexicon('Mitch','Mitch',np,_).
tag_lexicon('Michael','Michael',np,_).
tag_lexicon('Marie','Marie',np,_).
tag_lexicon(mal,mal,ad,_).
tag_lexicon(maison,maison,n,_).
tag_lexicon(lentement,lentement,ad,_).
tag_lexicon('Kathy','Kathy',np,_).
tag_lexicon('Jean','Jean',np,_).
tag_lexicon(glace,glace,n,_).
tag_lexicon('France','France',n,_).
tag_lexicon(demain,demain,ad,_).
tag_lexicon(de,de,p,_).
tag_lexicon(dans,dans,p,_).
tag_lexicon(contre,contre,p,_).
tag_lexicon(cin�ma,cin�ma,n,_).
tag_lexicon(chez,chez,p,_).
tag_lexicon('Bonnie','Bonnie',np,_).
tag_lexicon(bient�t,bient�t,ad,_).
tag_lexicon(bien,bien,ad,_).
tag_lexicon('B�atrice','B�atrice',np,_).
tag_lexicon(beaucoup,beaucoup,ad,_).
tag_lexicon(avec,avec,p,_).
tag_lexicon('Anne','Anne',np,_).
tag_lexicon('Andrew','Andrew',np,_).
tag_lexicon(�,�,p,_).
tag_lexicon(et,et,coord,_).
