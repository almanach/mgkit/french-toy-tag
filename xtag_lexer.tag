/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2000, 2004 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  xtag_main.tag -- French Toy XTAG (non lexicalized)
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-include 'xtag_header.tag'.

:-require
	'xtag_lexicon.tag',
	'xtag_lemma.tag',
	'format.pl'
	.

?-recorded(L::'N'(_)),format('~q.\n',[L]),fail.

?- recorded('C'(L,TokenCatFam,R)),
   (   TokenCatFam = Token:CatFam xor Token=TokenCatFam ),
   (   CatFam = Cat:Family xor Cat = CatFam ),
   (   recorded(tag_lexicon(Token,_,_,_)) ->
       recorded(tag_lexicon(Token,Lemma,Cat,Top)),
       normalized_tag_lemma(Lemma,Cat,Family,VarCoanchors,VarEquations),
       normalize_coanchors(VarCoanchors)
   ;   
       true % unknown word
   ),
   New_C = 'C'(L,
	       lemma{ lex => Token,
		      cat => Cat,
		      top => Top,
		      lemma => Lemma,
		      anchor => tag_anchor{ name => Family,
					    coanchors => VarCoanchors,
					    equations => VarEquations
					  }
		    },
	       R),
   optimized_format([New_C]^'~q.\n'),
   fail
   .
