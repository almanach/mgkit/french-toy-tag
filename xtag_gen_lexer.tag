/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2000 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  xtag_main.tag -- French Toy XTAG (non lexicalized)
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-include 'xtag_header.tag'.

:-require
	'xtag_lexicon.tag',
	'xtag_lemma.tag',
	'format.pl'
	.



?- 
   recorded(tag_lexicon(Token,Lemma,Cat,Top)),
   normalized_tag_lemma(Lemma,Cat,Family,VarCoanchors,VarEquations),
   normalize_coanchors(VarCoanchors),
   optimized_format([Token,Cat,Family,Lemma,Top,VarEquations,VarCoanchors]^'~w_~w\t~w\t~w\t~q\t~q\t~w\n'),
   fail
   .









