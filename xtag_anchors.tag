/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2000 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  xtag_anchors.tag -- Anchors for French Toy XTAG (Non Lexicalized)
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

%% :-tag_anchor{ name => Fam_Name,
%%               coanchors => Coanchors,      %% default = []
%%               equations => Equations      %% default = []
%%             }
%%           
%% <equation> := VarList^[ ((bot|top) = <feature>)+ at <nodeid> ] 

:-features( tag_anchor, [name,coanchors, equations] ).

:-tag_anchor{ name => ts1,
	      coanchors => [ np_0 ],
	      equations => [ [A]^(bot=v{ mode => A } at v_),
			     [B]^(bot=s{ mode => B } at s_1),
			     [C]^(top=np{ restr => C } at np_0)
			   ]
	    }.

:-tag_anchor{ name => tpn1,
	      coanchors => [ p_1 ],
	      equations => [ [A]^(top=np{ restr => A } at np_0),
			     [B]^(top=np{ restr => B } at np_1)
			   ]
	    }.

:-tag_anchor{ name => tn1pn2,
	      coanchors => [ p_2 ],
	      equations => [ [A]^(top=np{ restr => A } at np_2),
			     [B]^(top=np{ restr => B } at np_0)
			   ]
	    }.

:-tag_anchor{ name => tpdn1,
	      coanchors => [ d_1, n_1, p_1 ],
	      equations => [ [A,B,C,D]^(top=d{ poss => A, num=>B, pers=>C, npers=>D } at d_1),
			     [E,F,G]^(top=np{ pers => E, num => F, restr => G } at np_0)
			   ]
	    }.

:-tag_anchor{ name => tn1,
	      coanchors => [ np_0 ],
	      equations => [ [A]^(top=np{ restr => A } at np_1),
			     [B]^(top=np{ restr => B } at np_0)
			   ]
	    }.

:-tag_anchor{ name => tdn1,
	      coanchors => [ n_1, d_1],
	      equations => [ [A,B,C,D]^( top=d{ poss => A, num=> B, npers=> C, pers=>D } at d_1),
			     [E,F,G,H]^( bot=np{ restr => E }
				       and top=np{ num => F, pers => G, restr => H }
				       at np_0)
			   ]
	    }.


:-tag_anchor{ name =>  t,
	      coanchors => [np_0],
	      equations => [[A,B]^(bot=np{ restr => A } and top=np{ restr => B } at np_0)]
	    }.

:-tag_anchor{ name =>  vvp,
	      equations => [[A]^(bot=vp{ mode => A } at vp_)]
	    }.

:-tag_anchor{ name => na,
	      equations => [[A,B]^(bot=n{ restr => A } and top=n{ restr => B } at n_)]
	    }.

:-tag_anchor{ name => vppn,
	      equations => [[A]^(top=np{ restr => A } at np_)]
	    }.

:-tag_anchor{ name => d,
	      equations => [ [A,B]^(bot=d{ wh => A, poss => B } at d_)]
	    }.

:-tag_anchor{ name =>  a,
	      equations => [[A]^(bot=ap{ restr => A } at ap_)]
	    }.

:-tag_anchor{ name => np,
	      equations => [[A,B,C,D]^(bot=np{ restr => A, wh =>B, num => C, gen => D } at np_)]
	    }.

:-tag_anchor{ name => npdn,
	      equations => [ [A,B]^(bot=n{ num => A, gen => B } at n_),
			     [C,D]^(bot=np{ wh => C, restr => D } at np_)
			   ]
	    }.

:-tag_anchor{ name => [ta,svcl,vad,an,advp,vpad,nppn,sad,ads,p,n] }.


