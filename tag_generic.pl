/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id: tag_generic.pl,v 1.2 2000/04/17 09:02:45 clerger Exp $
 * Copyright (C) 2000, 2004, 2005, 2006, 2010 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  tag_generic.pl -- Generic Stuff for TAGs
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-scanner(check_lexical).

:-parse_mode(token).

:-features( tag_tree, [ family, name, tree ] ).
:-features( tag_anchor, [name, coanchors, equations ] ).

:-features( lemma, [lex,lemma,cat,top,anchor] ).

:-rec_prolog normalized_tag_lemma/5.
:-std_prolog anchor/6.
:-extensional tag_lexicon/4.

:-light_tabular
	verbose!tree/1,
	verbose!anchor/7,
	verbose!coanchor/5,
	verbose!lexical/5,
	verbose!struct/2,
	verbose!adj/0.

:-require('format.pl').

anchor(tag_anchor{ name => Family,
		   coanchors => VarCoanchors,
		   equations => VarEquations
		 },
       Token,
       Cat,
       Left,
       Right,
       Top
      ) :-
	'C'( Left,
	     lemma{ lex=> Token,
		    cat => Cat,
		    top => Top,
		    lemma => Lemma,
		    anchor => tag_anchor{ name => Family,
					  coanchors => Coanchors,
					  equations => VarEquations
					}
		  },
	     Right
	   ),
	(var(Lemma) xor check_all_coanchors(VarCoanchors,Coanchors)),
	true
	.

:-rec_prolog check_all_coanchors/2.

check_all_coanchors([],[]).

check_all_coanchors([VarLemma|VL],[Lemma|L]) :-
	( Lemma == [] ->
	    (	VarLemma = [] xor true)
	;   var(VarLemma) ->
	    VarLemma = Lemma
	;   VarLemma = check_at_anchor(Left,Right) ->
	    check_coanchor_lemma(Lemma,Left,Right)
	;   
	    fail
	),
	check_all_coanchors(VL,L).

:-std_prolog check_coanchor/3.

check_coanchor(VarLemma,Left,Right) :-
	( var(VarLemma) ->
	    VarLemma = check_at_anchor(Left,Right)
	;   VarLemma == [] ->
	    true
	;   
	    check_coanchor_lemma(VarLemma,Left,Right)
	)
	.

:-std_prolog check_coanchor_in_anchor/2.

check_coanchor_in_anchor(VarLemma,VarLemma).

:-std_prolog check_coanchor_lemma/3.

check_coanchor_lemma(Lemma,Left,Right) :-
	domain(Token,Lemma),
	'C'(Left,lemma{ lex=>Token },Right)
	.

:-std_prolog check_lexical/3.

check_lexical(Left,Token,Right) :-
	'C'(Left,lemma{ lex => Token },Right).

%% Dummy predicate used to leave a trace in the forest
verbose!tree(_Tree_Name) :-
%%	format('Verbose tree ~w\n',[_Tree_Name]),
	true
	.

%% Dummy predicate used to leave a trace in the forest
verbose!anchor(_Anchor,_Left,_Right,_Tree_Name,_Top,[_Lemma,_Lexical],_) :-
%%	format('Try anchor ~w left=~w right=~w tree=~w\n',[_Lexical,_Left,_Right,_Tree_Name]),		
	'C'(_Left,
	   lemma{
		 lex => _Lexical,
		 lemma=> _Lemma,
		 top => _Top
		},
	    _Right),
%%	format('Found anchor ~w left=~w right=~w\n',[_Lexical,_Left,_Right]),
	true.

%% Dummy predicate used to leave a trace in the forest
verbose!coanchor(_Lex,_Left,_Right,_Top,[_Lemma,_Lexical]) :-
%%	format('Try coanchor ~w left=~w right=~w\n',[_Lexical,_Left,_Right]),
        'C'(_Left,
	   lemma{ top => _Top,
		  cat => _Cat,
		  lex => _Lexical,
		  lemma => _Lemma},
	    _Right),
%%	format('Found coanchor ~w left=~w right=~w\n',[_Lexical,_Left,_Right]),
	true.


%% Dummy predicate used to leave a trace in the forest
verbose!lexical([_Lexical],_Left,_Right,_Cat,[_Lemma,_Lexical]) :-
%%	format('Try lexical ~w left=~w right=~w\n',[_Lexical,_Left,_Right]),
	'C'(_Left,lemma{ lex=> _Lexical, lemma => _Lemma, cat => _Cat }, _Right),
%%	format('Found lexical ~w left=~w right=~w\n',[_Lexical,_Left,_Right]),
	true
	.

%% Dummy predicate used to leave a trace in the forest
verbose!struct(_Tree_Name,HT).

%% Dummy predicate used to leave a trace in the forest
verbose!adj.

:-rec_prolog normalize_coanchors/1.

normalize_coanchors([]).
normalize_coanchors([VarCoanchors|VC]) :-
	(VarCoanchors = [] xor true ),
	normalize_coanchors(VC)
	.

:-std_prolog tag_family_load/7.

tag_family_load(Family,Cat,_Top,Token,_Name,_Left,_Right) :-
	'C'(_Left,
	    lemma{ cat => Cat,
		   anchor => tag_anchor{ name => Family }
		 },
	    _Right)
	xor fail.




