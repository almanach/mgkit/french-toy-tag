/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2000, 2004 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  xtag_lemma.tag -- Lemma for French Toy XTAG (Non Lexicalized)
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */


%% tag_lemma(<lemma>,<cat>,[<anchor>+])
%% <anchor> := tag_anchor{ name=> <Name>,
%%                         coanchors => [<coanchor>*]   %% default=[]
%%                         equations => [<equation>*]   %% default=[]
%%                         }
%% <coanchor> := [<nodeid>=<value>*]         %% several monovalued coanchors
%%             | [<nodeid>=(<value>*)]       %% one multivalued coanchor
%% <equation> := top=<term> and bot=<term> at <nodeid>

%% Check: every nodeid should appear in the anchor declaration for the family
%%        otherwise warning

:-include 'xtag_header.tag'.

tag_lemma('Yves',np,
	  tag_anchor{ name=>np,
		      equations=>[bot = np{ gen=>masc, num=>sing, restr=>plushum, wh=> (-) } at np_]}
	 ).

tag_lemma(vraiment,ad,
	  tag_anchor{ name=>[advp,sad,vad] }
	 ).

tag_lemma('*VRAI*',a,
	  [ tag_anchor{ name=>an },
	    tag_anchor{ name=>na,
			equations=>[top = n{ restr=>moinshum } at n_]}
	  ]
	 ).

tag_lemma('*VOIR*',v,
	  [ tag_anchor{ name=>tn1},
	    tag_anchor{ name=>ts1,
			equations=>[top = np{ restr=>plushum } at np_0,
				    bot = s{ mode=>ind } at s_1]},
	    tag_anchor{ name=>tdn1,
			coanchors=>[d_1=le,
				    n_1=loup]}
	  ]
	 ).

tag_lemma('*VISITER*',v,
	  tag_anchor{ name=>tn1,
		      equations=>[top = np{ restr=>moinshum } at np_1]}
	 ).

tag_lemma(vite,ad,
	  tag_anchor{ name=>vpad }
	 ).

tag_lemma('*VIN*',n,
	  tag_anchor{ name=>npdn,
		      equations=>[bot = np{ restr=>moinshum, wh=> (-) } at np_]}
	 ).

tag_lemma(vers,p,
	  tag_anchor{ name=>[p,vppn] }
	 ).

tag_lemma('*VENDRE*',v,
	  tag_anchor{ name=>tn1pn2,
		      coanchors=>[p_2=�],
		      equations=>[top = np{ restr=>plushum } at np_0,
				  top = np{ restr=>plushum } at np_2]}
	 ).

tag_lemma('*UN*',d,
	  tag_anchor{ name=>d,
		      equations=>[bot = d{ poss=> (-) } at d_]}
	 ).

tag_lemma('*TRAVAILLER*',v,
	  tag_anchor{ name=>t,
		      equations=>[top = np{ restr=>plushum } at np_0]}
	 ).

tag_lemma(tard,ad,
	  [ tag_anchor{ name=>vpad },
	    tag_anchor{ name=>vad }
	  ]
	 ).

tag_lemma('*TABLE*',n,
	  tag_anchor{ name=>npdn,
		      equations=>[bot = np{ restr=>moinshum, wh=> (-) } at np_]}
	 ).

tag_lemma(sur,p,
	  tag_anchor{ name=>[p,vppn,nppn] }
	 ).

tag_lemma('St�phane',np,
	  tag_anchor{ name=>np,
		      equations=>[bot = np{ gen=>masc, num=>sing, restr=>plushum, wh=> (-) } at np_]}
	 ).

tag_lemma(souvent,ad,
	  tag_anchor{ name=>sad }
	 ).

tag_lemma(sous,p,
	  tag_anchor{ name=> [p,vppn, nppn] }
	 ).

tag_lemma('*SON*',d,
	  tag_anchor{ name=>d,
		      equations=>[bot = d{ poss=> (+) } at d_]}
	 ).

tag_lemma('Sharon',np,
	  tag_anchor{ name=>np,
		      equations=>[bot = np{ gen=>fem, num=>sing, restr=>plushum, wh=> (-) } at np_]}
	 ).

tag_lemma('*SEMBLER*',v,
	  [ tag_anchor{ name=>ta },
	    tag_anchor{ name=>vvp,
			equations=>[bot = vp{ mode=>inf } at vp_]}
	  ]
	 ).

tag_lemma('*SAVOIR*',v,
	  tag_anchor{ name=>ts1,
		      equations=>[bot = s{ mode => mode[ind,inf]} at s_1,
				  top = np{ restr=>plushum } at np_0]}
	 ).

tag_lemma('Sabine',np,
	  tag_anchor{ name=>np,
		      equations=>[bot = np{ gen=>fem, num=>sing, restr=>plushum, wh=> (-) } at np_]}
	 ).

tag_lemma('*ROUGE*',a,
	  [ tag_anchor{ name=>a },
	    tag_anchor{ name=>na }
	  ]
	 ).

tag_lemma('*REGARDER*',v,
	  tag_anchor{ name=>tn1,
		      equations=>[top = np{ restr=>plushum } at np_0]}
	 ).

tag_lemma('*QUEL*',d,
	  tag_anchor{ name=>d,
		      equations=>[bot = d{ wh=> (+) } at d_]}
	 ).

tag_lemma(que,np,
	  tag_anchor{ name=>np,
		      equations=>[bot = np{ restr=>moinshum, wh=> (+) } at np_]}
	 ).

tag_lemma(qui,np,
	  tag_anchor{ name=>np,
		      equations=>[bot = np{ restr=>plushum, wh=> (+) } at np_]}
	 ).

tag_lemma(quoi,np,
	  tag_anchor{ name=>np,
		      equations=>[bot = np{ restr=>moinshum, wh=> (+) } at np_]}
	 ).

tag_lemma(pour,p,
	  tag_anchor{ name=> [p,vppn,nppn] }
	 ).

tag_lemma('*POUVOIR*',v,
	  [ tag_anchor{ name=>vvp,
			equations=>[bot = vp{ mode=>inf } at vp_]},
	    tag_anchor{ name=>vvp,
			equations=>[bot = vp{ mode=>inf } at vp_]}
	  ]
	 ).

tag_lemma('*PLEUVOIR*',v,
	  tag_anchor{ name=>t,
		      coanchors=>[np_0=il]}
	 ).

tag_lemma('*PETIT*',a,
	  [ tag_anchor{ name=>an },
	    tag_anchor{ name=>na }
	  ]).

tag_lemma('*PERDRE*',v,
	  [ tag_anchor{ name=>tn1},
	    tag_anchor{ name=>tdn1,
			coanchors=>[d_1=les,
				    n_1=p�dales],
			equations=>[top = np{ restr=>plushum } at np_0]}
	  ]
	 ).

tag_lemma('*PENSER*',v,
	  tag_anchor{ name=>ts1,
		      equations=>[ bot = s{ mode=>ind } at s_1,
				   top = np{ restr=>plushum } at np_0]}
	 ).

tag_lemma('*PEDALE*',n,
	  [tag_anchor{ name=>npdn,
		      equations=>[bot = np{ restr=>moinshum, wh=> (-) } at np_]},
	   tag_anchor{ name=>n }
	  ]
	 ).

tag_lemma('*PARTIR*',v,
	  [ tag_anchor{ name=>t },
	    tag_anchor{ name=>tpn1,
			coanchors=>[p_1= (�;de;dans)],
			equations=>[top = np{ restr=>moinshum } at np_1]}
	  ]
	 ).

tag_lemma('*PARLER*',v,
	  [ tag_anchor{ name=>t },
	    tag_anchor{ name=>tpn1,
			coanchors=>[p_1=avec],
			equations=>[ top = np{ restr=>plushum } at np_0,
				     top = np{ restr=>plushum } at np_1]},
	    tag_anchor{ name=>tpdn1,
			coanchors=>[p_1=dans,
				    n_1=barbe],
			equations=>[ top = np{ num=>X1, pers=>X2, restr=>plushum } at np_0,
				     top = d{ npers=>X1, num=>pl, pers=>X2, poss=> (+) } at d_1]}
	  ]
	 ).

tag_lemma('Paris',np,
	  tag_anchor{ name=>np,
		      equations=>[bot = np{ restr=>moinshum, wh=> (-) } at np_]}
	 ).

tag_lemma('*PARFUM*',n,
	  tag_anchor{ name=>npdn,
		      equations=>[bot = np{ restr=>moinshum, wh=> (-) } at np_]}
	 ).

tag_lemma(parfois,ad,
	  tag_anchor{ name=> [advp,vad] }
	 ).

tag_lemma(ou,ad,
	  tag_anchor{ name=>ads }
	 ).

tag_lemma(on,np,
	  tag_anchor{ name=>np,
		      equations=>[bot = np{ restr=>plushum } at np_]}
	 ).

tag_lemma('*MONTRER*',v,
	  tag_anchor{ name=>tn1pn2,
		      coanchors=>[p_2=�],
		      equations=>[top = np{ restr=>plushum } at np_2]}
	 ).

tag_lemma('*MONTRE*',n,
	  tag_anchor{ name=>npdn,
		      equations=>[bot = np{ restr=>moinshum, wh=> (-) } at np_]}
	 ).

tag_lemma('Mitch',np,
	  tag_anchor{ name=>np,
		      equations=>[bot = np{ gen=>masc, num=>sing, restr=>plushum, wh=> (-) } at np_]}
	 ).

tag_lemma('Michael',np,
	  tag_anchor{ name=>np,
		      equations=>[bot = np{ gen=>masc, num=>sing, restr=>plushum, wh=> (-) } at np_]}
	 ).

tag_lemma('*METTRE*',v,
	  [ tag_anchor{ name=>tn1pn2,
			coanchors=>[p_2= (dans;sur)],
			equations=>[top = np{ restr=>plushum } at np_0,
				    top = np{ restr=>moinshum } at np_2]},
	    tag_anchor{ name=>tdn1,
			coanchors=>[d_1=le,
				    n_1=paquet]}
	  ]
	 ).

tag_lemma('*MERE*',n,
	  tag_anchor{ name=>npdn,
		      equations=>[bot = np{ restr=>plushum, wh=> (-) } at np_]}
	 ).

tag_lemma('Marie',np,
	  tag_anchor{ name=>np,
		      equations=>[bot = np{ gen=>fem, num=>sing, restr=>plushum, wh=> (-) } at np_]}
	 ).

tag_lemma('*MANGER*',v,
	  [ tag_anchor{ name=>t },
	    tag_anchor{ name=>tn1,
			equations=>[ top = np{ restr=>moinshum } at np_1]},
	    tag_anchor{ name=>tdn1,
			coanchors=>[n_1=mots],
			equations=>[ top = np{ num=>X1, pers=>X2, restr=>plushum } at np_0,
				     top = d{ npers=>X1, num=>pl, pers=>X2, poss=> (+) } at d_1]},
	    tag_anchor{ name=>tpdn1,
			coanchors=>[p_1=avec,
				    n_1='lance-pierre',
				    d_1=un],
			equations=>[top = np{ restr=>plushum } at np_0]}
	  ]
	 ).

tag_lemma(mal,ad,
	  tag_anchor{ name=>vad }
	 ).

tag_lemma(maison,n,
	  tag_anchor{ name=>npdn,
		      equations=>[bot = n{ gen=>fem, num=>sing } at n_,
				  bot = np{ restr=>moinshum, wh=> (-) } at np_]}
	 ).

tag_lemma('*LIVRE*',n,
	  tag_anchor{ name=>npdn,
		      equations=>[bot = np{ restr=>moinshum } at np_]}
	 ).

tag_lemma('*LIRE*',v,
	  [ tag_anchor{ name=>t },
	    tag_anchor{ name=>tn1,
			equations=>[top = np{ restr=>plushum } at np_0,
				    top = np{ restr=>moinshum } at np_1]}
	  ]
	 ).

tag_lemma('*LEUR*',d,
	  tag_anchor{ name=>d,
		      equations=>[bot = d{ poss=> (+) } at d_]}
	 ).

tag_lemma(lentement,ad,
	  tag_anchor{ name=> [vpad,vad] }
	 ).

tag_lemma('*LE*',d,
	  tag_anchor{ name=>d,
		      equations=>[bot = d{ poss=> (-) } at d_]}
	 ).

tag_lemma('Kathy',np,
	  tag_anchor{ name=>np,
		      equations=>[bot = np{ gen=>fem, num=>sing, restr=>plushum, wh=> (-) } at np_]}
	 ).

tag_lemma('*JOUER*',v,
	  [ tag_anchor{ name=>t,
			equations=>[top = np{ restr=>plushum } at np_0]},
	    tag_anchor{ name=>tpn1,
			coanchors=>[p_1=�],
			equations=>[top = np{ restr=>plushum } at np_0]}
	  ]
	 ).

tag_lemma('*JOLI*',a,
	  [ tag_anchor{ name=>an },
	    tag_anchor{ name=>na },
	    tag_anchor{ name=>a }
	  ]
	 ).

tag_lemma('Jean',np,
	  tag_anchor{ name=>np,
		      equations=>[bot = np{ gen=>masc, num=>sing, restr=>plushum, wh=> (-) } at np_]}
	 ).

tag_lemma('*INTELLIGENT*',a,
	  [ tag_anchor{ name=>a,
			equations=>[bot = ap{ restr=>plushum } at ap_]},
	    tag_anchor{ name=>na,
			equations=>[top = n{ restr=>plushum } at n_]}
	  ]
	 ).

tag_lemma('*IL*',n,
	  tag_anchor{ name=>np,
		      equations=>[bot = np{ wh=> (-) } at np_]}
	 ).

tag_lemma('*HOMME*',n,
	  tag_anchor{ name=>npdn,
		      equations=>[bot = np{ wh=> (-) } at np_]}
	 ).

tag_lemma('*GRAND*',a,
	  tag_anchor{ name=> [na,an,a] }
	 ).

tag_lemma(glace,n,
	  tag_anchor{ name=>npdn,
		      equations=>[bot = n{ gen=>fem } at n_,
				  bot = np{ restr=>moinshum, wh=> (-) } at np_]}
	 ).

tag_lemma('*GARCON*',n,
	  tag_anchor{ name=>npdn,
		      equations=>[bot = np{ restr=>plushum, wh=> (-) } at np_]}
	 ).

tag_lemma('France',n,
	  tag_anchor{ name=>npdn,
		      equations=>[bot = n{ gen=>fem } at n_,
				  bot = np{ restr=>moinshum, wh=> (-) } at np_]}
	 ).

tag_lemma('*FLEUR*',n,
	  tag_anchor{ name=>npdn,
		      equations=>[bot = np{ restr=>moinshum, wh=> (-) } at np_]}
	 ).

tag_lemma('*FILLE*',n,
	  [ tag_anchor{ name=>npdn,
		      equations=>[bot = np{ restr=>plushum, wh=> (-) } at np_]},
	    tag_anchor{ name=>n }
	  ]
	 ).

tag_lemma('*FEMME*',n,
	  tag_anchor{ name=>npdn,
		      equations=>[bot = np{ restr=>plushum, wh=> (-) } at np_]}
	 ).

tag_lemma('*FALLOIR*',v,
	  tag_anchor{ name=>[ts1,tn1],
		      coanchors=>[np_0=il]}
	 ).

tag_lemma('*ESP�RER*',v,
	  [ tag_anchor{ name=>tn1,
			equations=>[top = np{ restr=>plushum } at np_0]},
	    tag_anchor{ name=>ts1,
			equations=>[top = np{ restr=>plushum } at np_0,
				    bot = s{ mode=>ind } at s_1]}
	  ]
	 ).

tag_lemma('*FAIRE*',v,
	  tag_anchor{ name=>tn1,
		      equations=>[top = np{ restr=>plushum } at np_0]}
	 ).

tag_lemma('*ETRE*',v,
	  [ tag_anchor{ name=> [ta,tn1] },
	    tag_anchor{ name=>tpn1,
			coanchors=>[p_1= (sur;�;dans;chez)]}
	  ]
	 ).

tag_lemma('*ENFANT*',n,
	  tag_anchor{ name=>npdn,
		      equations=>[bot = np{ restr=>plushum, wh=> (-) } at np_]}
	 ).

tag_lemma('*ELLE*',np,
	  tag_anchor{ name=>np }
	 ).

tag_lemma('*DU*',d,
	  tag_anchor{ name=>d,
		      equations=>[bot = d{ poss=> (-) } at d_]}
	 ).

tag_lemma('*DORMIR*',v,
	  tag_anchor{ name=>t,
		      equations=>[top = np{ restr=>plushum } at np_0]}
	 ).

tag_lemma('*DONNER*',v,
	  tag_anchor{ name=>tn1pn2,
			coanchors=>[p_2=�],
			equations=>[top = np{ restr=>plushum } at np_0,
				    top = np{ restr=>plushum } at np_2]}
	 ).

tag_lemma('*DIRE*',v,
	  tag_anchor{ name=>ts1,
		      equations=>[%bot = v{ mode=>inf } at v_,   %% WARNING
				  top = np{ restr=>plushum } at np_0,
				  bot = s{ mode => mode[ind,subj]} at s_1]}
	 ).

tag_lemma('*DEVOIR*',v,
	  tag_anchor{ name=>vvp,
		      equations=>[bot = vp{ mode=>inf } at vp_]}
	 ).

tag_lemma(demain,ad,
	  tag_anchor{ name=>vpad }
	 ).

tag_lemma(de,p,
	  tag_anchor{ name=> [p,nppn] }
	 ).

tag_lemma(dans,p,
	  tag_anchor{ name=> [p,vppn] }
	 ).

tag_lemma('*CROIRE*',v,
	  [ tag_anchor{ name=>tn1},
	    tag_anchor{ name=>ts1,
			equations=>[bot = s{ mode=>ind } at s_1,
				    top = np{ restr=>plushum } at np_0]}
	  ]
	 ).

tag_lemma('*COURIR*',v,
	  [ tag_anchor{ name=>tdn1,
			coanchors=>[d_1=les,
				    n_1=filles],
			equations=>[bot = np{ restr=>plushum } at np_0]},
	    tag_anchor{ name=>t },
	    tag_anchor{ name=>tpn1,
			coanchors=>[p_1= (�;apr�s;vers)],
			equations=>[top = np{ restr=>plushum } at np_0]}
	  ]
	 ).

tag_lemma(contre,p,
	  tag_anchor{ name=>[p,nppn,vppn] }
	 ).

tag_lemma('*COMPRENDRE*',v,
	  [ tag_anchor{ name=>t,
			equations=>[bot = np{ restr=>plushum } at np_0]},
	    tag_anchor{ name=>ts1,
			equations=>[top = np{ restr=>plushum } at np_0,
				    bot = s{ mode => mode[ind,subj]} at s_1]}
	  ]
	 ).

tag_lemma(cin�ma,n,
	  tag_anchor{ name=>npdn,
		      equations=>[bot = n{ gen=>masc, num=>sing } at n_,
				  bot = np{ restr=>moinshum, wh=> (-) } at np_]}
	 ).

tag_lemma('*CHIEN*',n,
	  tag_anchor{ name=>npdn,
		      equations=>[bot = np{ wh=> (-) } at np_]}
	 ).

tag_lemma(chez,p,
	  tag_anchor{ name=>vppn,
		      equations=>[top = np{ restr=>plushum } at np_]}
	 ).

tag_lemma('*CHERCHER*',v,
	  tag_anchor{ name=> [tn1,t] }
	 ).

tag_lemma('*CHAT*',n,
	  tag_anchor{ name=>npdn,
		      equations=>[bot = np{ wh=> (-) } at np_]}
	 ).

tag_lemma('*CE*',d,
	  tag_anchor{ name=>d,
		      equations=>[bot = d{ poss=> (-) } at d_]}
	 ).

tag_lemma('*BRISER*',v,
	  [ tag_anchor{ name=>tdn1,
			coanchors=>[d_1=la,
				    n_1=glace]},
	    tag_anchor{ name=>tn1,
			equations=>[top = np{ restr=>moinshum } at np_1]}
	  ]
	 ).

tag_lemma('Bonnie',np,
	  tag_anchor{ name=>np,
		      equations=>[bot = np{ gen=>fem, num=>sing, restr=>plushum, wh=> (-) } at np_]}
	 ).

tag_lemma('*BON*',a,
	  tag_anchor{ name=> [an,na,a] }
	 ).

tag_lemma('*BOIRE*',v,
	  [ tag_anchor{ name=>tn1,
			equations=>[top = np{ restr=>plushum } at np_0]},
	    tag_anchor{ name=>t,
			equations=>[top = np{ restr=>plushum } at np_0]},
	    tag_anchor{ name=>tdn1,
			coanchors=>[d_1=un,
				    n_1=coup],
			equations=>[top = np{ restr=>plushum } at np_0]},
	    tag_anchor{ name=>tpdn1,
			coanchors=>[p_1=comme,
				    d_1=un,
				    n_1=trou],
			equations=>[top = np{ restr=>plushum } at np_0]}
	  ]
	 ).

tag_lemma('*BLEU*',a,
	  tag_anchor{ name=>[na,a] }
	 ).

tag_lemma(bient�t,ad,
	  tag_anchor{ name=>vpad }
	 ).

tag_lemma(bien,ad,
	  tag_anchor{ name=>vad }
	 ).

tag_lemma('*BETE*',a,
	  [ tag_anchor{ name=>a,
			equations=>[bot = ap{ restr=>plushum } at ap_]},
	    tag_anchor{ name=>na,
			equations=>[bot = n{ restr=>plushum } at n_]}
	  ]
	 ).

tag_lemma('B�atrice',np,
	  tag_anchor{ name=>np,
		      equations=>[bot = np{ gen=>fem, num=>sing, restr=>plushum, wh=> (-) } at np_]}
	 ).

tag_lemma(beaucoup,ad,
	  tag_anchor{ name=> [vad,vpad] }
	 ).

tag_lemma('*BEAU*',a,
	  tag_anchor{ name=> [a,na,an] }
	 ).

tag_lemma(avec,p,
	  tag_anchor{ name=> [p,nppn,vppn] }
	 ).

tag_lemma('Anne',np,
	  tag_anchor{ name=>np,
		      equations=>[bot = np{ gen=>fem, num=>sing, restr=>plushum, wh=> (-) } at np_]}
	 ).

tag_lemma('Andrew',np,
	  tag_anchor{ name=>np,
		      equations=>[bot = np{ gen=>masc, num=>sing, restr=>plushum, wh=> (-) } at np_]}
	 ).

tag_lemma('*AMI*',n,
	  tag_anchor{ name=>npdn,
		      equations=>[bot = np{ restr=>plushum, wh=> (-) } at np_]}
	 ).

tag_lemma('*AM�RICAIN*',a,
	  tag_anchor{ name=> [na,a] }
	 ).

tag_lemma('*ALLER*',v,
	  tag_anchor{ name=>tpn1,
		      coanchors=>[p_1= (�;chez;vers)],
		      equations=>[top = np{ restr=>plushum } at np_0]}
	 ).

tag_lemma('*AIMER*',v,
	  tag_anchor{ name=>tn1,
		      equations=>[top = np{ restr=>plushum } at np_0]}
	 ).

tag_lemma('*AVOIR*',v,
	  tag_anchor{ name=>tn1}
	 ).

tag_lemma(�,p,
	  tag_anchor{ name=> [p,nppn,vppn] }
	  ).


tag_lemma(et,coord,
	  tag_anchor{ name => coord }
	 ).
